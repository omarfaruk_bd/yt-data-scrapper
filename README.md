# YoutubeDataScrapper
Youtube Data Scrapper is written in python3 using django framework. It scrapes data using google youtube v3 api.

## Installation 
packages requried for the project are listed in requirements.txt, User the package manager [pip](https://pip.pypa.io/en/stable) to install the requirements


```bash
pip install -r requirements.txt 
```



## Usage
to run the django application use the following command:


```bash
python manage.py runserver
```
## Configure the scrapper
    application home page will give a box for user to input the channel id or channel id list in comma separated string.
    if you want to scrape data from multiple channel then mark the check box and give your list of channel id in the box.

    In the Scheduler  Settings page your can configure the scheduler update interval time and can start/stop the scheduler.


## Documents
    flowchart diagrams and other documentations can be found in docs/ directory