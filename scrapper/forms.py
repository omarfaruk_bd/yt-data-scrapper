import re
from django import forms
from django.forms import ModelForm

scheduler_choices = [("start", "Start Schedule Updater"), ("stop", "Stop Schedule Updater")]
interval_choices = [(2, "2 Minute"), (4, "4 Minute"), (5, "5 Minute"), (10, "10 Minute"), (12, "12 Minute"), (15, "15 Minute"),
                    (20, "20 Minute"), (30, "30 Minute")]


class ChannelScrapper(forms.Form):
    multiple_channel = forms.BooleanField(label="Comma Seperated Multiple Channel ID", required=False)
    channel_id = forms.CharField(max_length=1024, required=False, label='Youtube Channel ID', widget=forms.Textarea)


class SchedulerConfigureFrom(forms.Form):
    scheduler_start_stop = forms.ChoiceField(required=True, choices=[(value, name) for value, name in scheduler_choices],
                                             label="Start or Stop Scheduler for Updating Data Automatically")

    update_interval = forms.ChoiceField(required=False, choices=[(value, name) for value, name in interval_choices])
