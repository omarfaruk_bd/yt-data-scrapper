from django.db import models


# Create your models here.

class Video(models.Model):
    video_id = models.CharField(max_length=64, primary_key=True, null=False, blank=True)
    video_title = models.CharField(max_length=1024, null=True, blank=True)
    publishedAt = models.DateTimeField(blank=True, null=True)
    channel_id = models.CharField(max_length=128, null=True, blank=True)
    tags = models.CharField(max_length=1024, null=True, blank=True)
    viewCount = models.IntegerField(null=False, blank=False, default=0)
    likeCount = models.IntegerField(null=False, blank=False, default=0)
    dislikeCount = models.IntegerField(null=False, blank=False, default=0)
    favoriteCount = models.IntegerField(null=False, blank=False, default=0)
    commentCount = models.IntegerField(null=False, blank=False, default=0)
    first_hour_view_count = models.IntegerField(null=False, blank=False, default=0)
    performance = models.FloatField(null=False, blank=False, default=0)

    class Meta:
        db_table = "videos"

class ChannelInfo(models.Model):
    channel_id = models.CharField(max_length=128, primary_key=True, null=False, blank=False)
    first_hour_view_median = models.FloatField(blank=False, null=False, default=0)
