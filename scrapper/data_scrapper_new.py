import json
import os
import threading

import pytz

from .models import Video, ChannelInfo
from datetime import datetime, timedelta, timezone
from googleapiclient import discovery
from django.conf import settings


class Scrapper:
    def __init__(self, channel_id=None, channel_list=None):
        self.channel_id = channel_id
        self.max_result = 50  # maximum results in a request
        self.channel_list = channel_list
        self.running = False
        self.error = []
        self.nth_hour_view = settings.NTH_HOUR_VIEW  # settings.py contains the hour value
        self.nth_hour_seconds = self.nth_hour_view * 3600

    def start(self):
        print("#######start scraping data#######")
        # if channel_list exists, then create a thread for each channel id
        if (self.channel_list is not None):
            for channel in self.channel_list:
                channel_thread = threading.Thread(target=self.scrape_data_by_search, args=[channel])
                channel_thread.start()
                # channel_thread.join()

        else:
            # if only one channel id is provided then make one function call only
            self.scrape_data_by_search(self.channel_id)

        return self.error

    def scrape_data_by_search(self, channel_id):
        # list the videos sorted by upload date
        # if the videos are older, we cannot count first hour views.
        # only pick the videos that was uploaded today
        youtube_interactor = discovery.build(settings.API_SERVICE_NAME,
                                             settings.API_VERSION,
                                             developerKey=settings.DEV_KEY)
        # print(channel_id)
        if (ChannelInfo.objects.filter(channel_id=channel_id).count() == 0):
            new_channel = ChannelInfo(channel_id=channel_id, first_hour_view_median=0)
            new_channel.save()
        try:
            self.running = True

            next_page_token = None

            current_time = datetime.utcnow()
            publish_after = current_time - timedelta(minutes=self.nth_hour_view * 60)
            publish_after = datetime.strftime(publish_after, "%Y-%m-%dT%H:%M:%SZ")
            # only fetch videos that was published during last n-hour, in hour case last hour

            while (True):
                request = youtube_interactor.search().list(part="snippet", channelId=channel_id,
                                                           order="date", maxResults=self.max_result,
                                                           publishedAfter=publish_after, pageToken=next_page_token)

                response = request.execute()

                # if publish time is less than more than 1 hour ago
                # we stop adding those videos into DB, because we cant calculate their first hour view count
                publishAt = response['items'][0]['snippet']['publishedAt']
                publishAt = datetime.strptime(publishAt, "%Y-%m-%dT%H:%M:%SZ")
                currentTime = datetime.utcnow()  # youtube publishAt is in UTC
                timeDelta = currentTime - publishAt

                # safety check if videos that was uploaded earlier than last hour
                if (timeDelta.seconds > self.nth_hour_seconds):  # more than nth_hour old
                    break

                self.extract_properties_and_store(response['items'], currentTime, youtube_interactor)

                next_page_token = response.get('nextPageToken')
                if (next_page_token is None):
                    break
            self.running = False

        except Exception as e:
            self.running = False
            self.error.append(e.__str__())

    def extract_properties_and_store(self, items, current_time, youtube_interactor):

        # youtube_interactor = discovery.build(settings.API_SERVICE_NAME,
        #                                      settings.API_VERSION,
        #                                      developerKey=settings.DEV_KEY)
        video_ids = ""

        videos = {}
        for item in items:
            # we're interested in videos only, no playlist
            # we discard playlist items by checking if the id attribute contains 'videoId"

            if (item['id'].get('videoId')):
                video_id = item['id']['videoId']
                video_title = item['snippet']['title']
                publishedAt = item['snippet']['publishedAt']
                channel_id = item['snippet']['channelId']

                publishedAt = datetime.strptime(publishedAt, "%Y-%m-%dT%H:%M:%SZ")

                timeDelta = current_time - publishedAt

                if (timeDelta.seconds > 3600):
                    continue

                video = {
                    "video_id": video_id,
                    "video_title": video_title,
                    "publishedAt": publishedAt,
                    'channel_id': channel_id
                }
                videos.update({
                    video_id: video
                })

                if (video_ids == ""):
                    video_ids = video_ids + video_id
                else:
                    video_ids = video_ids + "," + video_id

        requests = youtube_interactor.videos().list(
            part="snippet,statistics", id=video_ids)

        response = requests.execute()

        for item in response['items']:

            videos[item['id']].update({
                "viewCount": item['statistics']['viewCount'],
                "likeCount": item['statistics']['likeCount'],
                "dislikeCount": item['statistics']['dislikeCount'],
                "favoriteCount": item['statistics']['favoriteCount'],
            })

            if (item['statistics'].get('commentCount')):
                # if the video commenting was not turned off
                videos[item['id']].update({"commentCount": item['statistics']['commentCount']})
            else:
                videos[item['id']].update({"commentCount": 0})

            if (item['snippet'].get('tags')):
                videos[item['id']].update({
                    "tags": item['snippet']['tags']
                })

            if (Video.objects.filter(video_id=item['id']).count() == 0):
                # only store new video
                new_video = Video(**videos[item['id']])
                new_video.first_hour_view_count = new_video.viewCount
                new_video.save()
                # print(new_video.video_id, "\n\n")
