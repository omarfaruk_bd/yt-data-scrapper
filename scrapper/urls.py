from django.urls import path,include
from scrapper import views,viewsets
from rest_framework import routers

routers = routers.DefaultRouter()

routers.register(r'videos', viewsets.VideoViewSets)


urlpatterns= [
    path('api/',include(routers.urls)),
    path('', views.channel_scraper, name="channel_scraper"),
    path('scheduler', views.scheduler, name="update_scheduler"),

]
