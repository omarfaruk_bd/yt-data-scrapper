import threading
from datetime import datetime, timedelta
from time import sleep
from scrapper.models import Video, ChannelInfo
from scrapper.data_scrapper_new import Scrapper

from googleapiclient import discovery
from django.conf import settings


class ScheduleUpdater:
    instance = None

    def __init__(self):

        self.update_interval = 300  # default update interval
        self.running = False  # default running state is stopped
        self.stop_signal = True
        self.channels_per_thread = 3
        self.nth_hour = settings.NTH_HOUR_VIEW
        self.nth_hour_minutes = self.nth_hour * 60
        ScheduleUpdater.instance = self

    @staticmethod
    def getIntance():

        if (ScheduleUpdater.instance is None):
            ScheduleUpdater()
        return ScheduleUpdater.instance

    def is_running(self):
        return self.running

    def start(self):
        self.stop_signal = False

        main_tread = threading.Thread(target=self.main_thread)
        main_tread.start()

    def stop(self):
        self.stop_signal = True

    def update_interval_settings(self, interval_mins):
        self.update_interval = interval_mins * 60

    def main_thread(self):

        while (True):
            if (self.stop_signal):
                break

            else:
                channels = ChannelInfo.objects.all()
                current_time = datetime.utcnow()

                for i in range(0, len(channels), self.channels_per_thread):
                    # group a N channel list, and start a thread for each N channels
                    channel_list = channels[i:i + self.channels_per_thread]

                    #self.channel_data_updater(channel_list, current_time)
                    thread = threading.Thread(target=self.channel_data_updater, args=[channel_list, current_time])
                    thread.start()


                    #start scrapping stored channels for newly uploaded videos
                    channel_list = []
                    for each in channels:
                         channel_list.append(each.channel_id)
                    new_scrapper = Scrapper(channel_list=channel_list)
                    new_scrapper.start()

            sleep(self.update_interval)

    def channel_data_updater(self, channel_list, current_time):

        youtube_interactor = discovery.build(settings.API_SERVICE_NAME,
                                                  settings.API_VERSION,
                                                  developerKey=settings.DEV_KEY)
        for channel in channel_list:
            publish_at = current_time - timedelta(minutes=self.nth_hour_minutes)
            videos = Video.objects.filter(channel_id=channel.channel_id, publishedAt__gte=publish_at)
            median = ChannelInfo.objects.get(channel_id=channel.channel_id).first_hour_view_median

            video_ids = ""
            for video in videos:
                if (video_ids == ""):
                    video_ids = video_ids + video.video_id
                else:
                    video_ids = video_ids + "," + video.video_id

            requests = youtube_interactor.videos().list(
                part="snippet,statistics", id=video_ids)

            try:
                response = requests.execute()
            except Exception as e:
                response = {
                    "items":[]
                }

            for item in response['items']:
                video = videos.get(video_id=item['id'])
                video.viewCount = int(item['statistics']['viewCount'])
                video.likeCount = int(item['statistics']['likeCount'])
                video.dislikeCount = int(item['statistics']['dislikeCount'])
                video.favoriteCount =  int(item['statistics']['favoriteCount'])

                if (item['statistics'].get('commentCount')):
                    # if the video commenting was not turned off
                    video.commentCount = int(item['statistics']['commentCount'])

                video.first_hour_view_count = video.viewCount
                video.save()
            channel_videos = Video.objects.filter(channel_id=channel.channel_id).order_by('first_hour_view_count')


            no_of_vids = channel_videos.count()




            if(no_of_vids > 1):
                if(no_of_vids % 2==0):
                    # if even number of videos, then average of n/2-th,n/2+1-th is the median
                    index = no_of_vids//2
                    index1 = index + 1
                    median = (channel_videos[index].first_hour_view_count+ channel_videos[index1].first_hour_view_count)/2
                else:
                    # if even number of videos, then first_hour_view_count of n/2-th is the median
                    index = no_of_vids // 2
                    median = channel_videos[index].first_hour_view_count

                print("updating channel info: ",channel.channel_id," median ",median)

                ChannelInfo.objects.filter(channel_id=channel.channel_id).update(first_hour_view_median=median)


                # update performance value of all videos of the channel
                videos = Video.objects.filter(channel_id=channel.channel_id)
                for video in videos:
                    video.performance = video.first_hour_view_count/median
                    video.save()
