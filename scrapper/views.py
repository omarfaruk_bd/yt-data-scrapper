from django.shortcuts import render
from scrapper.data_scrapper_new import Scrapper
from scrapper.update_scheduler import ScheduleUpdater
from scrapper.forms import ChannelScrapper, SchedulerConfigureFrom


def channel_scraper(request):
    if (request.method == 'GET'):
        ch_form = ChannelScrapper()
        return render(request, "scrapper.html", {"form": ch_form})

    if (request.method == 'POST'):
        ch_form = ChannelScrapper(request)
        channel_id = request.POST.get('channel_id')
        if (channel_id is not None):
            if(request.POST.get("multiple_channel")):
                channel_list = channel_id.split(',')
                new_channel_scrapper = Scrapper(channel_list=channel_list)
            else:
                new_channel_scrapper = Scrapper(channel_id=channel_id)
            error = new_channel_scrapper.start()

            print(error)
            if(error):
                ch_form = ChannelScrapper()
                return render(request, "scrapper.html", {"form": ch_form, "errors": error})
            else:
                ch_form = ChannelScrapper()
                return render(request, "scrapper.html", {"form": ch_form, "success": "success"})


def scheduler(request):
    config_form = SchedulerConfigureFrom()
    if(request.method =='GET'):
        return render(request, "scheduler.html", {"form": config_form})
    else:
        start_stop = request.POST.get("scheduler_start_stop")
        interval = request.POST.get("update_interval")
        if(start_stop == "start"):
            scheduler = ScheduleUpdater.getIntance()
            scheduler.update_interval_settings(int(interval))
            scheduler.start()

        if(start_stop == "stop"):
            scheduler = ScheduleUpdater.getIntance()
            scheduler.stop()

        return render(request, "scheduler.html", {"form": config_form, "success": "success"})
