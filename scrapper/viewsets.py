from rest_framework.decorators import action
from scrapper.models import Video
from scrapper.serializers import VideoSerializer1
from rest_framework import viewsets
from django.db.models import Q
from rest_framework.response import Response


class VideoViewSets(viewsets.ModelViewSet):
    serializer_class = VideoSerializer1
    queryset = Video.objects.all()

    @action(detail=False)
    def tags(self, request):
        tag_list = []
        if(request.query_params.get('P')):
            tag_list = request.query_params['P'].split(',')
        query = Q()
        for tag in tag_list:
            query |= Q(tags__icontains=tag)

        videos = Video.objects.filter(query)

        serializer = VideoSerializer1(videos, many=True, context={'request': request})

        return Response(serializer.data)


    @action(detail=False)
    def performance(self,request):

        performance_value = 1
        if(request.query_params.get('P')):
            performance_value = float(request.query_params['P'])
        videos = Video.objects.filter(performance__gt = performance_value)
        serializer = VideoSerializer1(videos, many=True, context={'request': request})
        return Response(serializer.data)