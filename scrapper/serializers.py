from scrapper.models import Video, ChannelInfo
from rest_framework import serializers


class VideoSerializer1(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Video
        fields = '__all__'

class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = '__all__'
